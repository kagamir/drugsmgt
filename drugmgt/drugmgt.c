﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BASE_FILE "./medicine_base.dat"
#define DEAL_FILE "./medicine_deal.dat"

typedef struct medicineInfo {
	char number[20];
	char name[20];
	char type[20];
	char manufacturer[20];
	float price;
	char exp[20];
	char note[20];
	int stock;
	struct medicineInfo* next;
} info;

typedef struct medicineDeal {
	char number[20];
	char name[20];
	float price;
	int quantity;
	float amount;
	char date[20];
	char type[5];       //交易类型: 采购/销售
	char operator[20];
	struct medicineDeal* next;
} deal;

enum STRUCT_KEY {
	INFO_NUMBER, INFO_NAME, INFO_TYPE, INFO_MANUFACTURER, INFO_PRICE, INFO_EXP, INFO_NOTE, INFO_STOCK,
	DEAL_NUMBER, DEAL_NAME, DEAL_PRICE, DEAL_QUANTITY, DEAL_AMOUNT, DEAL_DATE, DEAL_TYPE, DEAL_OPERATOR
};


char fetchKey() {
	char c, ch;
	c = getchar();
	while ((ch = getchar()) != '\n' && ch != EOF);
	return c;
}

int isFileEnd(FILE* fp) {
	getc(fp);
	int end = feof(fp);
	fseek(fp, -1L, SEEK_CUR);
	return end;
}


void init() {
	FILE* fp1 = fopen(BASE_FILE, "r");
	if (!fp1) {
		fp1 = fopen(BASE_FILE, "w");
	}
	fclose(fp1);

	FILE* fp2 = fopen(DEAL_FILE, "r");
	if (!fp2) {
		fp2 = fopen(DEAL_FILE, "w");
	}
	fclose(fp2);


	
}

// 读取文件成链表
info* baseRead() {
	FILE* fp = fopen(BASE_FILE, "rb");
	if (fp == NULL) {
		printf("[%s 无法打开]\n", BASE_FILE);
		return NULL;
	}
	else if(isFileEnd(fp)){
		return NULL;
	}
	info* header = (info*)malloc(sizeof(info));
	info* ptr = header;
	while (!isFileEnd(fp)) {
		info* linked = (info*)malloc(sizeof(info));
		fread(linked, sizeof(info), 1, fp);
		ptr->next = linked;
		ptr = ptr->next;
	}
	ptr->next = NULL;
	fclose(fp);
	return header;
}

deal* dealRead() {
	FILE* fp = fopen(DEAL_FILE, "rb");
	if (fp == NULL) {
		printf("[%s 无法打开]\n", DEAL_FILE);
		return NULL;
	}
	else if (isFileEnd(fp)) {
		return NULL;
	}
	deal* header = (deal*)malloc(sizeof(deal));
	deal* ptr = header;
	while (!isFileEnd(fp)) {
		deal* linked = (deal*)malloc(sizeof(deal));
		fread(linked, sizeof(deal), 1, fp);
		ptr->next = linked;
		ptr = ptr->next;
	}
	ptr->next = NULL;
	fclose(fp);
	return header;
}


// 把药品信息链表存到文件
void baseWrite(info* header, char mode) {
	FILE* fp = NULL;
	if (mode == 'a') {
		fp = fopen(BASE_FILE, "ab+");
	}
	else if (mode == 'f') {
		fp = fopen(BASE_FILE, "wb+");
	}
	
	if (fp == NULL) {
		printf("[%s 无法打开]\n", BASE_FILE);
		return;
	}

	char headerFlag = 1;

	while (header != NULL){
		info* temp = header;
		header = header->next;
		temp->next = NULL;
		if (!headerFlag) {
			fwrite(temp, sizeof(info), 1, fp);
		}
		free(temp);
		headerFlag = 0;
	}
	fclose(fp);
	printf("[信息录入成功]\n");
}



void dealWrite(deal* header) {
	FILE* fp = fopen(DEAL_FILE, "ab+");
	if (fp == NULL) {
		printf("[%s 无法打开]\n", DEAL_FILE);
		return;
	}
	char headerFlag = 1;

	while (header != NULL) {
		deal* temp = header;
		header = header->next;
		temp->next = NULL;
		if (!headerFlag) {
			fwrite(temp, sizeof(deal), 1, fp);
		}
		free(temp);
		headerFlag = 0;
	}
	fclose(fp);
	printf("[交易录入成功]\n");
}

// 当i==0时，表明是链表头部，不显示链表头部信息，转而显示输出信息表的表头。
void showInfoNode(info* node, int i) {
	if (i == 0) {
		printf("\n[药品信息]:\n");
		printf("\t编号      种类      名称           厂家           价格      有效期       库存      说明\n");
		printf("-----------------------------------------------------------------------------------------------------------\n");	
	}
	else {
		printf("%d.\t%-10s%-10s%-15s%-15s%-10.2f%-13s%-10d%s\n", i,
			node->number, node->type, node->name, node->manufacturer, node->price, node->exp, node->stock, node->note
		);
	}
}


// 查看药品信息链表
// 无需跳过链表头部，让showInfoNode来处理。
void showInfoLinked(info* header) {
	for (int i = 0; header != NULL; i++) {
		showInfoNode(header, i);
		header = header->next;
	}
}


void showDealNode(deal* node, int i) {
	if (i == 0) {
		printf("\n[交易信息]:\n");
		printf("\t编号      名称           单价      数量      金额         时间         操作员    类型\n");
		printf("-----------------------------------------------------------------------------------------------------------\n");
	}
	else {
		printf("%d.\t%-10s%-15s%-10.2f%-10d%-13.2f%-13s%-10s%s\n", i,
			node->number, node->name, node->price, node->quantity, node->amount, node->date, node->operator, node->type
		);
	}
}



// 查看🔍交♂易信息链表⛓
void showDealLinked(deal* header) {
	for (int i = 0; header != NULL; i++) {
		showDealNode(header, i);
		header = header->next;
	}
}


// 查找链表
info* findInfoLinked(info* header, char num[]) {
	header = header->next;
	for (; header; header=header->next) {
		if (strcmp(header->number, num) == 0) {
			return header;
		}
	}
	return NULL;
}

deal* findDealLinked(deal* header, char num[]) {
	header = header->next;
	for (; header; header = header->next) {
		if (strcmp(header->number, num) == 0) {
			return header;
		}
	}
	return NULL;
}




void byebyeInfoLinked(info* header) {
	// TODO
}





// 1. 录入药品基本信息
void inputInfo() {

	info* header = (info*)malloc(sizeof(info));
	info* ptr = header;
	info* currentHeader = baseRead();
	do {
		char num[20];
		printf("\n输入药品编号：");
		scanf("%s", num);
		getchar();
		if (currentHeader && findInfoLinked(currentHeader, num)) {
			printf("[编号重复，请确认]\n");
			return;
		}
		info* row = (info*)malloc(sizeof(info));
		row->next = NULL;
		row->stock = 0;
		sprintf(row->number, "%s", num);

		printf("输入药品名称：");
		scanf("%s", row->name);
		getchar();
		printf("输入药品种类：");
		scanf("%s", row->type);
		getchar();
		printf("输入药品厂家：");
		scanf("%s", row->manufacturer);
		getchar();
		printf("输入药品价格：");
		scanf("%f", &row->price);
		getchar();
		printf("输入药品有效期：");
		scanf("%s", row->exp);
		getchar();
		printf("输入药品说明：");
		scanf("%s", row->note);
		getchar();

		ptr->next = row;
		ptr = ptr->next;
		printf("\n是否继续添加(y/n)? ");

	} while (fetchKey() == 'y');
	
	printf("以下内容即将添加：\n");
	showInfoLinked(header);
	printf("\n是否录入(y/n)? ");
	if (fetchKey()=='y'){
		baseWrite(header, 'a');
	}
	else {
		printf("[您已放弃录入]\n");
	}
	

	
}

// 2/3. 药品的入库管理
void dealManagement(char mode) {
	// 语料
	char type[10];
	if (mode == 'i') {
		sprintf(type, "%s", "采购");
	}
	else if (mode == 'o') {
		sprintf(type, "%s", "销售");
	}

	info* infoHeader = baseRead();
	if (infoHeader == NULL) {
		printf("\n[药品基本信息为空，无法%s]\n", type);
		return;
	}
	deal* dealHeader = (deal*)malloc(sizeof(deal));
	dealHeader->next = NULL;
	deal* ptr = dealHeader;
	do {
		printf("输入药品编号：");
		char num[20];
		scanf("%s", num);
		getchar();
		info* target = findInfoLinked(infoHeader, num);
		if (!target) {
			printf("[药品信息表中找不到，请确认编号]\n");
			continue;
		}
		showInfoNode(NULL, 0);
		showInfoNode(target, 1);
		printf("输入%s数量：", type);
		int stock = 0;
		scanf("%d", &stock);
		getchar();
		if (mode == 'i') {
			target->stock += stock;
		}
		else if (mode == 'o') {
			if (target->stock >= stock) {
				target->stock -= stock;
			}
			else {
				printf("\n[库存不足!]\n");
				continue;
			}
		}
		deal* dealNode = (deal*)malloc(sizeof(deal));
		dealNode->next = NULL;

		sprintf(dealNode->number, "%s", target->number);
		sprintf(dealNode->name, "%s", target->name);
		dealNode->price = target->price;
		sprintf(dealNode->type, "%s", type);

		dealNode->quantity = stock;
		dealNode->amount = target->price * stock;

		printf("输入%s时间：", type);
		scanf("%s", dealNode->date);
		getchar();
		printf("输入操作员：");
		scanf("%s", dealNode->operator);
		getchar();

		ptr->next = dealNode;
		ptr = ptr->next;

	} while (printf("\n是否继续(y/n)? ") && fetchKey() == 'y');

	if (dealHeader->next != NULL) {
		baseWrite(infoHeader, 'f');
		dealWrite(dealHeader);
	}
	else {
		free(infoHeader);
		free(dealHeader);
		printf("[返回]\n");
	}
}




// 4. 修改药品基本信息
void editInfo() {
	info* infoHeader = baseRead();
	printf("输入药品编号：");
	char num[20];
	scanf("%s", num);
	getchar();
	info* target = findInfoLinked(infoHeader, num);
	if (!target) {
		printf("[药品信息表中找不到，请确认编号]");
		return;
	}
	showInfoNode(NULL, 0);
	showInfoNode(target, 1);
	printf("\n1.种类 2.厂家 3.价格 4.有效期 5.说明\n输入1-5来修改对应项：");
	switch (fetchKey()) {
	case '1':
		printf("输入药品种类：");
		scanf("%s", target->type);
		getchar();
		break;
	case '2':
		printf("输入药品厂家：");
		scanf("%s", target->manufacturer);
		getchar();
		break;
	case '3':
		printf("输入药品价格：");
		scanf("%f", &target->price);
		getchar();
		break;
	case '4':
		printf("输入药品有效期：");
		scanf("%s", target->exp);
		getchar();
		break;
	case '5':
		printf("输入药品说明：");
		scanf("%s", target->note);
		getchar();
		break;
	default:
		printf("[取消]\n");
		return;
	}
	baseWrite(infoHeader, 'f');

}

void deleteInfo() {
	info* infoHeader = baseRead();
	if (infoHeader == NULL) {
		printf("\n[药品基本信息已经为空]\n");
		return;
	}
	printf("输入药品编号：");
	char num[20];
	scanf("%s", num);
	getchar();

	deal* dealHeader = dealRead();

	if (dealHeader != NULL && findDealLinked(dealHeader, num) != NULL) {
		printf("[交易信息表中存在，无法删除]\n");
		return;
	}

	info* infoTarget = findInfoLinked(infoHeader, num);
	if (infoTarget == NULL) {
		printf("[药品信息表中找不到，无法删除，请确认编号]\n");
		return;
	}
	
	showInfoNode(NULL, 0);
	showInfoNode(infoTarget, 1);
	printf("确定删除 %s (y/n)?", infoTarget->name);
	if (fetchKey() == 'y') {
		info* ptr = infoHeader;
		while (ptr->next) {
			if (strcmp(ptr->next->number, num) == 0) {
				info* temp = ptr->next;
				ptr->next = ptr->next->next;
				free(temp);
				baseWrite(infoHeader, 'f');
				printf("[删除完成]\n");
				return;
			}
			ptr = ptr->next;
		}
		printf("[删除失败，未知错误]\n");
		return;
	}
	else {
		printf("[放弃删除]\n");
		return;
	}
}


// 5. 输出全部药品基本信息、药品交易信息
void showAll() {
	info* infoHeader = baseRead();
	if (infoHeader) {
		showInfoLinked(infoHeader);
	}
	else {
		printf("\n[药品基本信息为空]\n");
	}
	deal* dealHeader = dealRead();
	if (dealHeader) {
		showDealLinked(dealHeader);
	}
	else {
		printf("\n[药品交易信息为空]\n");
	}
		
}


void* getInfoNodeValue(info* node, char key) {
	switch (key) {
	case INFO_NUMBER:
		return node->number;
	case INFO_NAME:
		return node->name;
	case INFO_TYPE:
		return node->type;
	case INFO_MANUFACTURER:
		return node->manufacturer;
	case INFO_PRICE:
		return &node->price;
	case INFO_EXP:
		return node->exp;
	case INFO_NOTE:
		return node->note;
	case INFO_STOCK:
		return &node->stock;
	default:
		printf("[DEBUG] getInfoNodeValue ERROR!\n");
		return NULL;
	}
}

void* getDealNodeValue(deal* node, char key) {
	switch (key) {
	case DEAL_NUMBER:
		return node->number;
	case DEAL_NAME:
		return node->name;
	case DEAL_PRICE:
		return node->type;
	case DEAL_QUANTITY:
		return &node->quantity;
	case DEAL_AMOUNT:
		return &node->amount;
	case DEAL_DATE:
		return node->date;
	case DEAL_TYPE:
		return node->type;
	case DEAL_OPERATOR:
		return &node->operator;
	default:
		printf("[DEBUG] getDealNodeValue ERROR!\n");
		return NULL;
	}
}

void searchInfo(char key, char text[]) {
	info* infoHeader = baseRead();
	if (!infoHeader) {
		printf("\n[药品基本信息为空]\n");
		return;
	}
	printf("请输入%s：", text);
	char str[20];
	scanf("%s", str);
	getchar();
	info* ptr = infoHeader->next;
	char flag = 0;
	showInfoNode(NULL, 0);
	for (int i = 1; ptr; ) {
		char* value = (char*)getInfoNodeValue(ptr, key);
		if (strcmp(value, str) == 0) {
			showInfoNode(ptr, i++);
			flag = 1;
		}
		ptr = ptr->next;
	}
	if (flag == 0) {
		printf("[未找到药品信息]\n");
	}
}

void searchDeal(char key, char text[]) {
	deal* dealHeader = dealRead();
	if (!dealHeader) {
		printf("\n[药品交易信息为空]\n");
		return;
	}
	printf("请输入%s：", text);
	char str[20];
	scanf("%s", str);
	getchar();
	deal* ptr = dealHeader->next;
	char flag = 0;
	showDealNode(NULL, 0);
	for (int i = 1; ptr; ) {
		char* value = getDealNodeValue(ptr, key);
		if (strcmp(value, str) == 0) {
			showDealNode(ptr, i++);
			flag = 1;
		}
		ptr = ptr->next;
	}
	if (flag == 0) {
		printf("[未找到药品信息]\n");
	}
}

void countInfo(char key, char text[]) {
	info* infoHeader = baseRead();
	if (!infoHeader) {
		printf("\n[药品基本信息为空]\n");
		return;
	}
	printf("请输入%s：", text);
	char str[20];
	scanf("%s", str);
	getchar();
	info* ptr = infoHeader->next;
	int stock = 0;
	char flag = 0;
	for (int i = 1; ptr; ) {
		char* value = (char*)getInfoNodeValue(ptr, key);
		if (strcmp(value, str) == 0) {
			stock += ptr->stock;
			flag = 1;
		}
		ptr = ptr->next;
	}
	if (flag == 0) {
		printf("[未找到药品信息]\n");
	}
	else {
		printf("[%s的库存有：%d]\n", str, stock);
	}
}


void countDeal(char key, char text[]) {
	deal* dealHeader = dealRead();
	if (!dealHeader) {
		printf("\n[药品交易信息为空]\n");
		return;
	}
	printf("请输入%s：", text);
	char str[20];
	scanf("%s", str);
	getchar();
	deal* ptr = dealHeader->next;
	int quantity = 0;
	float amount = 0;
	char flag = 0;
	for (int i = 1; ptr; ) {
		char* value = (char*)getDealNodeValue(ptr, key);
		if (strcmp(value, str) == 0) {
			quantity += ptr->quantity;
			//amount += ptr->quantity * ptr->price;
			amount += ptr->amount;
			flag = 1;
		}
		ptr = ptr->next;
	}
	if (flag == 0) {
		printf("[未找到交易信息]\n");
	}
	else {
		printf("[\n交易数量为：%d\n交易金额为：%.2f\n]\n", quantity, amount);
	}
}



// 6. 查询管理
void searchMgt() {
	printf("1.查询药品基本信息\n2.查询交易信息\n>>>");
	char k = fetchKey();
	if (k == '1') {
		printf("1.名称\n2.种类\n3.厂家\n4.有效期\n>>>");
		switch (fetchKey()) {
		case '1':
			searchInfo(INFO_NAME, "名称");
			break;
		case '2':
			searchInfo(INFO_TYPE, "种类");
			break;
		case '3':
			searchInfo(INFO_MANUFACTURER, "厂家");
			break;
		case '4':
			searchInfo(INFO_EXP, "有效期");
			break;
		default:
			break;
		}
	}
	else if (k == '2') {
		printf("1.名称\n2.时间\n3.交易类型\n4.操作员\n>>>");
		switch (fetchKey()) {
		case '1': 
			searchDeal(DEAL_NAME, "名称");
			break;		
		case '2': 
			searchDeal(DEAL_DATE, "时间");
			break;		
		case '3': 
			searchDeal(DEAL_TYPE, "交易类型");
			break;		
		case '4': 
			searchDeal(DEAL_OPERATOR, "操作员");
			break;		
		default:
			break;
		}
	}
}


// 7. 统计管理
void countMgt() {
	printf("1.统计药品基本信息\n2.统计交易信息\n>>>");
	char k = fetchKey();
	if (k == '1') {
		printf("1.名称\n2.种类\n3.厂家\n4.有效期\n>>>");
		switch (fetchKey()) {
		case '1':
			countInfo(INFO_NAME, "名称");
			break;
		case '2':
			countInfo(INFO_TYPE, "种类");
			break;
		case '3':
			countInfo(INFO_MANUFACTURER, "厂家");
			break;
		case '4':
			countInfo(INFO_EXP, "有效期");
			break;
		default:
			break;
		}
	}
	else if (k == '2') {
		printf("1.名称\n2.时间\n3.交易类型\n4.操作员\n>>>");
		switch (fetchKey()) {
		case '1':
			countDeal(DEAL_NAME, "名称");
			break;
		case '2':
			countDeal(DEAL_DATE, "名称");
			break;
		case '3':
			countDeal(DEAL_TYPE, "交易类型");
			break;
		case '4':
			countDeal(DEAL_OPERATOR, "操作员");
			break;
		default:
			break;
		}
	}


}



int main() {
	printf("药品管理系统\n\t\t学号：19951060000\n");
	init();
	for (;;) {
		printf("\n\n======================================\n\n \
1) 录入药品基本信息\n \
2) 药品的入库管理\n \
3) 药品的销售管理\n \
4) 修改药品基本信息\n \
5) 输出全部药品基本信息、药品交易信息\n \
6) 查询管理\n \
7) 统计管理\n \
8) 退出系统\n\n");

		printf(">>>");

		switch (fetchKey())
		{
		case '1':
			inputInfo();
			break;
		case '2':
			dealManagement('i');
			break;
		case '3':
			dealManagement('o');
			break;
		case '4':
			printf("\n1. 添加\n2. 修改\n3. 删除\n输入其他返回\n>>>");
			switch (fetchKey()) {
			case '1':
				inputInfo();
				break;
			case '2':
				editInfo();
				break;
			case '3':
				deleteInfo();
				break;
			default:
				printf("\n[返回]\n");
				break;
			}
			break;
		case '5': {
			showAll();
			break;
		}
		case '6':
			searchMgt();
			break;
		case '7':
			countMgt();
			break;
		case '8':
			exit(0);
		default:
			printf("\n[请输入数字1-8]\n");
			break;
		}
	}
	return -1;
}


